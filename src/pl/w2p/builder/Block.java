package pl.w2p.builder;

import lombok.Data;

@Data
public class Block {
    String floor;
    String colour;

    public Block() {
    }

    @Override
    public String toString() {
        return "pl.w2p.old.version.Block{" +
                "floor='" + floor + '\'' +
                ", colour='" + colour + '\'' +
                '}';
    }
}
