package pl.w2p.builder;

public class Main {

    public static void main(String[] args) {

        BankAccount bankAccount = new BankAccount
                .BankAccountBuilder("Jon", "22738022275")
                .withEmail("jon@example.com")
                .wantNewsletter(true)
                .withStreet("ba street")
                .withBlockColour("red")
                .build();

        System.out.println("pl.w2p.old.version.Main.main.bankAccount: " + bankAccount);

        BankAccountCiti bankAccountCiti = new BankAccountCitiBuilder()
                .withAddress(new Address("s", "c", new Block()))
                .withCity("city")
                .withStreet("bac street")
                //.withBankAccount(new pl.w2p.old.version.BankAccount())
                .withName("Citi account")
                .createBankAccountCiti();

        System.out.println("pl.w2p.old.version.Main.main.bankAccountCiti: " + bankAccountCiti);
    }
}
