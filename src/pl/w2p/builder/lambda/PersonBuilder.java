package pl.w2p.builder.lambda;

import java.util.function.Consumer;

/**
 * think-functional-advanced-builder-pattern-using-lambda
 */
public class PersonBuilder {
    public String salutation;
    public String firstName;
    public String middleName;
    public String lastName;
    public String suffix;
    public Address address;
    public boolean isFemale;
    public boolean isEmployed;
    public boolean isHomewOwner;

    public PersonBuilder with(Consumer<PersonBuilder> builderFunction) {
        builderFunction.accept(this);
        return this;
    }

    public Person build() {
        return new Person(salutation, firstName, middleName,
                lastName, suffix, address, isFemale,
                isEmployed, isHomewOwner);
    }


    /**
     * nested address builder
     */
    public static class AddressBuilder {

        public String street;
        public String city;
        public Other other;

        public AddressBuilder with(Consumer<AddressBuilder> builderFunction) {
            builderFunction.accept(this);
            return this;
        }

        public Address build() {
            return new Address(street, city, other);
        }


        /**
         * other nested builder
         */
        public static class OtherBuilder {
            public String other1;
            public String other2;

            public OtherBuilder with(Consumer<OtherBuilder> builderFunction) {
                builderFunction.accept(this);
                return this;
            }

            public Other build() {
                return new Other(other1, other2);
            }
        }
    }
}