package pl.w2p.builder.lambda;

public class Main {

    public static void main(String[] args) {
        Person person = new PersonBuilder()
                .with($ -> {
                    $.salutation = "Mr.";
                    $.firstName = "John";
                    $.middleName = "aaaa";
                    $.lastName = "Doe";
                    $.isFemale = false;
                    $.isHomewOwner = true;
                    $.address =
                            new PersonBuilder.AddressBuilder()
                                    .with($_address -> {
                                        $_address.city = "Pune";
                                        $_address.street = "MH";
                                        $_address.other = new PersonBuilder.AddressBuilder.OtherBuilder()
                                                .with($_other -> {
                                                    $_other.other1 = "other1111";
                                                    $_other.other2 = "other2222";
                                                }).build();
                                    }).build();
                })
                .build();

        System.out.println("Main.main.person: " + person);
    }
}


