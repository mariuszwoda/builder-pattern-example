package pl.w2p.builder.lambda;

public class Address {

    private String city;
    private String street;
    private Other other;

    public Address(String city, String street, Other other) {
        this.city = city;
        this.street = street;
        this.other = other;
    }

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", other='" + other + '\'' +
                '}';
    }
}
