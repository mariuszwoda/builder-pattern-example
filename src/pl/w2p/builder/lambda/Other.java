package pl.w2p.builder.lambda;

public class Other {

    public String other1;
    public String other2;

    public Other(String other1, String other2) {
        this.other1 = other1;
        this.other2 = other2;
    }
}
