package pl.w2p.builder;

public class BankAccountCitiBuilder {
    private String name;
    private Address address;
    private BankAccount bankAccount;

    public BankAccountCitiBuilder withName(String name) {
        this.name = name;
        return this;
    }


    public BankAccountCitiBuilder withCity(String city){
        this.address.setCity(city);
        return this;
    }

    public BankAccountCitiBuilder withStreet(String street){
        this.address.setStreet(street);
        return this;
    }

    public BankAccountCitiBuilder withAddress(Address address) {

        this.address = address;
        return this;
    }

    public BankAccountCitiBuilder withBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
        return this;
    }

    public BankAccountCiti createBankAccountCiti() {
        return new BankAccountCiti(name, address, bankAccount);
    }
}