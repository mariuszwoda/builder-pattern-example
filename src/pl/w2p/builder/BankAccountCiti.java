package pl.w2p.builder;

public class BankAccountCiti {

    private String name;
    private Address address;
    private BankAccount bankAccount;

    public BankAccountCiti(String name, Address address, BankAccount bankAccount) {
        this.name = name;
        this.address = address;
        this.bankAccount = bankAccount;
    }

    @Override
    public String

    toString() {
        return "pl.w2p.old.version.BankAccountCiti{" +
                "name='" + name + '\'' +
                ", address=" + address +
                ", bankAccount=" + bankAccount +
                '}';
    }
}
