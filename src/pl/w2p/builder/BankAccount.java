package pl.w2p.builder;

public class BankAccount {

    private String name;
    private String accountNumber;
    private String email;
    private Address address;
    private Block block;
    private boolean newsletter;

    //The constructor that takes a builder from which it will create object
    //the access to this is only provided to builder
    private BankAccount(BankAccountBuilder builder) {
        this.name = builder.name;
        this.accountNumber = builder.accountNumber;
        this.email = builder.email;
        this.newsletter = builder.newsletter;
        this.address = builder.address;
        this.block = builder.block;
    }

    @Override
    public String toString() {
        return "BankAccountBuilder{" +
                "name='" + name + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", email='" + email + '\'' +
                ", newsletter=" + newsletter +
                ", address=" + address +
                ", block=" + block +
                '}';
    }

    public static class BankAccountBuilder {

        private String name;
        private String accountNumber;
        private String email;
        private boolean newsletter;
        private Address address = new Address();
        private Block block = new Block();

        public BankAccountBuilder(String name, String accountNumber) {
            this.name = name;
            this.accountNumber = accountNumber;
        }

        public BankAccountBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public BankAccountBuilder wantNewsletter(boolean newsletter) {
            this.newsletter = newsletter;
            return this;
        }

        public BankAccountBuilder withStreet(String street) {
            this.address.setStreet(street);
            return this;
        }

        public BankAccountBuilder withBlockColour(String colour) {
            this.block.setColour("green");
            this.address.setBlock(this.block);
            return this;
        }

        public BankAccount build() {
            return new BankAccount(this);
        }

    }
}