package pl.w2p.builder;

import lombok.Data;

@Data
public class Address {

    private String street;
    private String city;
    private Block block;

    public Address(String street, String city, Block block) {
        this.street = street;
        this.city = city;
        this.block = block;
    }

    public Address() {
    }

    @Override
    public String toString() {
        return "pl.w2p.old.version.Address{" +
                "street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", block'" + block + '\'' +
                '}';
    }

}
